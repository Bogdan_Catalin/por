%{
#include <stdio.h>
extern FILE* yyin;
extern char* yytext;
extern int yylineno;
%}
%token BGIN END
%token LIST_BGIN LIST_END
%token ID FID TIP  
%token NR STRING LIST STRUCT CONST 
%token ACCESS_STRUCT
%token MAIN
%token COMMENT
%token PLUS_OPERATOR MINUS_OPERATOR MUL_OPERATOR DIV_OPERATOR MOD_OPERATOR ASSIGN
%token OR_OPERATOR AND_OPERATOR LT_OPERATOR LTE_OPERATOR EQ_OPERATOR GT_OPERATOR GTE_OPERATOR
%token IF WHILE FOR
%token RETURN

%start program
%%


program
	: declaratii MAIN bloc {printf ("Sintaxa programului este corecta\n");}
	;


comentariu
	: COMMENT //{printf ("Comentariu la linia %d\n", yylineno);}
	;

/*Accesarea unei structuri*/
access_struct
    : ACCESS_STRUCT ID
    | ACCESS_STRUCT ID access_struct
    ;
access_list
    : LIST_BGIN NR LIST_END
    | LIST_BGIN ID LIST_END
    | LIST_BGIN apel_functie LIST_END

    //Recursie
    | LIST_BGIN NR LIST_END access_list
    | LIST_BGIN ID LIST_END access_list
    | LIST_BGIN FID '(' lista_param ')' LIST_END access_list
    | LIST_BGIN FID '(' ')' LIST_END access_list
    ;
/* o linie de cod
   dupa fiecare item din aceasta lista, trebuie pus ;  */
declaratie
//asignari
	: ID ASSIGN ID {printf("bloc_func"); fflush(stdout);}
	| ID ASSIGN NR
	| ID ASSIGN exp_arit
	| ID ASSIGN STRING
	| ID ASSIGN lista
	| ID ASSIGN apel_functie

	| ID access_struct ASSIGN ID
	| ID access_struct ASSIGN NR
	| ID access_struct ASSIGN exp_arit
	| ID access_struct ASSIGN STRING
	| ID access_struct ASSIGN lista
	| ID access_struct ASSIGN apel_functie

	//Acces list
	| ID access_list

	//initializari
	| TIP ID
	| TIP ID ASSIGN NR
	| TIP ID ASSIGN exp_arit
	| TIP ID ASSIGN STRING
	| LIST ID
	| LIST ID ASSIGN lista //{printf ("Initializam o lista\n");}

	| CONST TIP ID
	| CONST TIP ID ASSIGN NR
	| CONST TIP ID ASSIGN exp_arit
	| CONST TIP ID ASSIGN STRING

	//functii
	//descriere
	| TIP FID '(' lista_param ')' bloc_func
	| TIP FID '(' ')' bloc_func
	| TIP FID '(' ')' BGIN END

	//apel
	| apel_functie


	//structuri de date
	| STRUCT ID BGIN declaratii END


	//if
	| IF '(' exp_bool ')' bloc
	//while
	| WHILE '(' exp_bool ')' bloc
	//for
	| FOR '(' for_parameters ')' bloc
	;


/* mai multe linii de cod */
declaratii
	: declaratie ';'
	| comentariu
	| declaratii declaratie ';'
	| declaratii comentariu
	;



/* linii de cod scrise intre { } */
bloc
	: BGIN declaratii END
	;

/* bloc de cod pentru functii cu return*/
// TO BE OR NOT TO BE
bloc_func
	: BGIN declaratii return END
	| BGIN declaratii END
	;

return
	: RETURN ID ';'
	| RETURN NR ';'
	| RETURN STRING ';'
	| RETURN lista ';'
	| RETURN ';'
	;

//parameters a for loop can have
for_parameters
	: NR ':' NR
	| ID ':' ID
	| NR ':' ID
	| ID ':' NR
	
	;


// FUNCTII
/* un singur parametru */


param 
	: TIP ID
	| CONST TIP ID
	| LIST ID
	| lista
	| ID
	| NR
	| STRING
	| FID '(' lista_param ')'
	| FID '(' ')'
	| exp_arit
	;

/* insiruire de parametri */
lista_param 
	: param
	| lista_param ','  param
	;


/*Apel functie*/
apel_functie
    : FID '(' lista_param ')'
    | FID '(' ')'
    ;


//LISTE
/* valorile ce pot fi atribuite unei liste */
element_din_lista
	: STRING //{printf ("String in list\n");}
	| NR //{printf ("Number in list\n");}
	| ID //{printf ("Variable in list\n");}
	| exp_arit
	| lista //{printf ("List in list\n");}
	;

lista_de_elemente
	: element_din_lista
	| lista_de_elemente ',' element_din_lista
	;

lista
	: LIST_BGIN lista_de_elemente LIST_END
	;


//EXPRESII
//aritmetice
exp_arit
	: operatii_arit
	| exp_arit operator_arit operatii_arit
	| exp_arit operator_arit ID
	| exp_arit operator_arit NR
	;

//operatii aritmetice
operatii_arit
	: ID operator_arit ID
	| ID operator_arit NR
	| NR operator_arit ID
	| NR operator_arit NR
	| operatii_arit operator_arit ID
	| operatii_arit operator_arit NR
	;

//operatori aritmetici
operator_arit
	: PLUS_OPERATOR 
	| MINUS_OPERATOR 
	| MUL_OPERATOR 
	| DIV_OPERATOR 
	| MOD_OPERATOR
	;

//expresii booleene
exp_bool
	: operatii_bool
	| exp_bool operator_bool NR
	| exp_bool operator_bool ID
	| exp_bool operator_bool exp_bool
	;

//operatii booleene
operatii_bool
	: ID operator_bool ID
	| ID operator_bool NR
	| NR operator_bool NR
	| NR operator_bool ID
	| operatii_bool operator_bool NR
	| operatii_bool operator_bool ID
	| exp_arit operator_bool exp_arit
	| exp_arit operator_bool ID
	| exp_arit operator_bool NR
	;

//operatori booleeni
operator_bool
	: OR_OPERATOR
	| AND_OPERATOR
	| LT_OPERATOR
	| LTE_OPERATOR
	| EQ_OPERATOR
	| GT_OPERATOR
	| GTE_OPERATOR	
	;

%%
int yyerror(char * s){
printf("eroare: %s la linia:%d\n",s,yylineno);
}

int main(int argc, char** argv){
yyin=fopen(argv[1],"r");
yyparse();
} 
