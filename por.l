%{
#include <stdio.h>
#include "y.tab.h"
%}
%%
"#".*?\n {unput('\n');return COMMENT;};

"+" {return PLUS_OPERATOR;}
"-" {return MINUS_OPERATOR;}
"*" {return MUL_OPERATOR;}
"/" {return DIV_OPERATOR;}
"%" {return MOD_OPERATOR;}

"||" {return OR_OPERATOR;}
"&&" {return AND_OPERATOR;}
"<" {return LT_OPERATOR;}
"<=" {return LTE_OPERATOR;}
"==" {return EQ_OPERATOR;}
">" {return GT_OPERATOR;}
">=" {return GTE_OPERATOR;}

"." {return ACCESS_STRUCT;}

"[" {return LIST_BGIN;}
"]" {return LIST_END;}

"if" {return IF;}
"while" {return WHILE;}
"for" {return FOR;}

"int"|"str" {return TIP;}
"const" {return CONST;}
"list" {return LIST;}
"struct" {return STRUCT;}

"return" {return RETURN;}

"{" {return BGIN;}
"}" {return END;}
"main" {return MAIN;}

@[_a-zA-Z][_a-zA-Z0-9]* {return ID;}
"$"[_a-zA-Z][_a-zA-Z0-9]* {return FID;}
":=" {return ASSIGN;}
[0-9]+|[0-9]+\.[0-9]* {return NR;}
\".*?\" {return STRING;}
[ \t] ;
\n {yylineno++;}
. {return yytext[0];}
